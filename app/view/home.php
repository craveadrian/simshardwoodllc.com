<div id="welcome">
	<div class="row">
		<div class="wlcLeft col-1 fl">
			<h1>WELCOME TO <span>Sims Hardwood LLC</span> </h1>
			<p>For all your hardwood flooring needs Sims Hardwood has you covered. Here at Sims Hardwood LLC, we can make your hardwood floors look as good as new. We have worked on many projects around the area and are known for our exceptional service. Get the quality work and friendly service that you deserve by calling Sims Hardwood LLC. Give us a call to learn more.</p>
			<a href="<?php echo URL ?>about#content" class="btn">READ MORE</a>
		</div>
		<div class="wlcRight col-2 fl">
			<img src="public/images/content/badge1.png" alt="Badge Image">
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="estimate">
		<div class="row">
			<p>Customer satisfaction guaranteed! <span>CALL TODAY FOR YOUR FREE ESTIMATE!</span></p>
		</div>
	</div>
</div>
<div id="services">
	<div class="row">
		<h1>OUR SERVICES</h1>
		<p>We are experts in our trade. Passion is what drives us everyday, trust us with your next project and you will experience what sets us apart. If you don’t see what you are looking for below give us a call or shoot us an email for a quote! </p>
		<div class="services-top">
			<dl>
				<dt>Installation</dt>
				<dd> <img src="public/images/content/svcImg1.jpg" alt="Installation"> </dd>
			</dl>
			<dl>
				<dt>Hardwood</dt>
				<dd> <img src="public/images/content/svcImg2.jpg" alt="Hardwood"> </dd>
			</dl>
		</div>
		<div class="services-bottom">
			<dl>
				<dt>Sanding</dt>
				<dd> <img src="public/images/content/svcImg3.jpg" alt="Sanding"> </dd>
			</dl>
			<dl>
				<dt>Pre-Finish</dt>
				<dd> <img src="public/images/content/svcImg4.jpg" alt="Pre-Finish"> </dd>
			</dl>
			<dl>
				<dt>Laminate</dt>
				<dd> <img src="public/images/content/svcImg5.jpg" alt="Laminate"> </dd>
			</dl>
		</div>
		<a href="services#content" class='btn'>VIEW MORE</a>
	</div>
</div>
<div id=about>
	<div class="row">
		<div class="about-text">
			<h1>ABOUT</h1>
			<p>Sims Hardwood is a local, family owned business for over 20 years. We strive for the best finished product along with honesty and professionalism. We install unfinished, pre-finished, laminate, sand, stain, and refinish. We also offer dust containment!</p>
			<a href="about#content" class="btn">LEARN MORE</a>
		</div>
	</div>
</div>
<div id="gallery">
	<div class="gallery-top">
		<div class="row">
			<h1>OUR GALLERY</h1>
			<p>Get the quality work and friendly service that you deserve by calling Sims Hardwood LLC. Give us a call to learn more.</p>
		</div>
	</div>
	<div class="gallery-bot">
		<div class="row">
			<div class="gallery-images">
				<img src="public/images/content/gallery1.jpg" alt="Gallery Image 1">
				<img src="public/images/content/gallery2.jpg" alt="Gallery Image 2">
				<img src="public/images/content/gallery3.jpg" alt="Gallery Image 3">
				<img src="public/images/content/gallery4.jpg" alt="Gallery Image 4">
				<img src="public/images/content/gallery5.jpg" alt="Gallery Image 5">
				<img src="public/images/content/gallery6.jpg" alt="Gallery Image 6">
			</div>
			<a href="gallery#content" class="btn">VIEW MORE</a>
		</div>
	</div>
</div>
<div id="logoMap">
	<div class="row">
		<a href="<?php echo URL ?>"> <img src="public/images/common/footLogo.png" alt="<?php $this->info("company_name"); ?> Logo"> </a>
	</div>
</div>
