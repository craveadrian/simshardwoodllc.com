<footer>
	<div id="footer">
		<div class="footHeader">
			<div class="row">
				<h1>CONTACT <span>US TODAY</span> </h1>
				<p>Please use the form on any page to contact us online. If you need immediate assistance, please call us!</p>
			</div>
		</div>
		<div class="footContent">
			<div class="footTop">
				<div class="row">
					<div class="contact-details">
						<dl>
							<dt> <img src="public/images/sprite.png" alt="Location Icon" class="bg-location"> <p>LOCATION</p> </dt>
							<dd><?php $this->info("address"); ?></dd>
						</dl>
						<dl>
							<dt> <img src="public/images/sprite.png" alt="Email Icon" class="bg-email"> <p>EMAIL</p> </dt>
							<dd><?php $this->info(["email", "mailto"]); ?></dd>
						</dl>
						<dl>
							<dt> <img src="public/images/sprite.png" alt="Phone Icon" class="bg-phone	"> <p>PHONE</p> </dt>
							<dd><?php $this->info(["phone", "tel"]); ?></dd>
						</dl>
					</div>
					<div class="contact-form">
						<form action="sendContactForm" method="post"  class="sends-email ctc-form" >
							<div class="left col-1 fl">
								<div class="txtLeft col-3 fl">
									<label><span class="ctc-hide">Full Name</span>
										<input type="text" name="name" placeholder="Full Name:">
									</label>
									<label><span class="ctc-hide">Address</span>
										<input type="text" name="address" placeholder="Address:">
									</label>
								</div>
								<div class="textRight col-3 fl">
									<label><span class="ctc-hide">Email</span>
										<input type="text" name="email" placeholder="Email:">
									</label>
									<label><span class="ctc-hide">Phone</span>
										<input type="text" name="phone" placeholder="Phone:">
									</label>
								</div>
								<div class="clearfix"></div>
								<label><span class="ctc-hide">Message</span>
									<textarea name="message" cols="30" rows="10" placeholder="Message:"></textarea>
								</label>
							</div>
							<div class="right col-2 fl">
								<div class="g-recaptcha"></div>
								<label>
									<input type="checkbox" name="consent" class="consentBox">I hereby consent to having this website store my submitted information so that they can respond to my inquiry.
								</label><br>
								<?php if( $this->siteInfo['policy_link'] ): ?>
								<label>
									<input type="checkbox" name="termsConditions" class="termsBox"/> I hereby confirm that I have read and understood this website's <a href="<?php $this->info("policy_link"); ?>" target="_blank">Privacy Policy.</a>
								</label>
								<?php endif ?>
								<button type="submit" class="ctcBtn btn" disabled>SUBMIT FORM</button>
							</div>
							<div class="clearfix"></div>
						</form>
					</div>
				</div>
			</div>
			<div class="footBot">
				<div class="row">
					<ul>
						<li <?php $this->helpers->isActiveMenu("home"); ?>><a href="<?php echo URL ?>">HOME</a></li>
						<li <?php $this->helpers->isActiveMenu("about"); ?>><a href="<?php echo URL ?>about">ABOUT US</a></li>
						<li <?php $this->helpers->isActiveMenu("services"); ?>><a href="<?php echo URL ?>services">SERVICES</a></li>
						<li <?php $this->helpers->isActiveMenu("gallery"); ?>><a href="<?php echo URL ?>gallery">GALLERY</a></li>
						<li <?php $this->helpers->isActiveMenu("contact"); ?>><a href="<?php echo URL ?>contact">CONTACT US</a></li>
					</ul>
					<p class="copy">
						<?php $this->info("company_name"); ?> © <?php echo date("Y"); ?>.  All Rights Reserved |
						<?php if( $this->siteInfo['policy_link'] ): ?>
							<a href="<?php $this->info("policy_link"); ?>">Privacy Policy</a>.
						<?php endif ?>
					</p>
					<p class="silver"><img src="public/images/scnt.png" alt="" class="company-logo" /><a href="https://silverconnectwebdesign.com/website-development" rel="external" target="_blank">Web Design</a> Done by <a href="https://silverconnectwebdesign.com" rel="external" target="_blank">Silver Connect Web Design</a></p>
					<div class="social">
						<a href="<?php $this->info("fb_link"); ?>" class="socialico">f</a>
						<a href="<?php $this->info("tt_link"); ?>" class="socialico">l</a>
						<a href="<?php $this->info("gp_link"); ?>" class="socialico">g</a>
						<a href="<?php $this->info("li_link"); ?>" class="socialico">i</a>
						<a href="<?php $this->info("yt_link"); ?>" class="socialico">x</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="<?php echo URL; ?>public/scripts/sendform.js" data-view="<?php echo $view; ?>" id="sendform"></script>
<!-- <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>  -->
<script src="<?php echo URL; ?>public/scripts/responsive-menu.js"></script>
<script src="https://unpkg.com/sweetalert2@7.20.10/dist/sweetalert2.all.js"></script>

<?php if( $this->siteInfo['cookie'] ): ?>
	<script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.js"></script>
	<script src="<?php echo URL; ?>public/scripts/cookie-script.js"></script>
<?php endif ?>

<?php if(in_array($view,["home","contact"])): ?>
	<script src='//www.google.com/recaptcha/api.js?onload=captchaCallBack&render=explicit' async defer></script>
	<script>
		var captchaCallBack = function() {
			$('.g-recaptcha').each(function(index, el) {
				grecaptcha.render(el, {'sitekey' : '<?php $this->info("site_key");?>'});
			});
		};

		$('.consentBox').click(function () {
		    if ($(this).is(':checked')) {
		    	if($('.termsBox').length){
		    		if($('.termsBox').is(':checked')){
		        		$('.ctcBtn').removeAttr('disabled');
		        	}
		    	}else{
		        	$('.ctcBtn').removeAttr('disabled');
		    	}
		    } else {
		        $('.ctcBtn').attr('disabled', true);
		    }
		});

		$('.termsBox').click(function () {
		    if ($(this).is(':checked')) {
	    		if($('.consentBox').is(':checked')){
	        		$('.ctcBtn').removeAttr('disabled');
	        	}
		    } else {
		        $('.ctcBtn').attr('disabled', true);
		    }
		});

	</script>

<?php endif; ?>


<?php if ($view == "gallery"): ?>
	<script type="text/javascript" src="<?php echo URL; ?>public/fancybox/source/jquery.fancybox.js?v=2.1.5"></script>
	<script type="text/javascript" src="<?php echo URL; ?>public/scripts/jquery.pajinate.js"></script>
	<script>
		$('#gall1').pajinate({ num_page_links_to_display : 3, items_per_page : 10 });
		$('.fancy').fancybox({
			helpers: {
				title : {
					type : 'over'
				}
			}
		});
	</script>
<?php endif; ?>

<a class="cta" href="tel:<?php $this->info("phone") ;?>"></a>

<?php $this->checkSuspensionFooter(); ?>
</body>
</html>
