<?php $this->suspensionRedirect($view); ?>
<!DOCTYPE html>
<html lang="en" <?php $this->helpers->htmlClasses(); ?>>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />

	<?php $this->helpers->seo($view); ?>
	<link rel="icon" href="public/images/favicon.png" type="image/x-icon">
	<!-- <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> -->
	<link href="<?php echo URL; ?>public/styles/style.css" rel="stylesheet">
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" />
	<link rel="stylesheet" href="<?php echo URL; ?>public/fancybox/source/jquery.fancybox.css" media="screen" />
	<link href="https://fonts.googleapis.com/css?family=Abhaya+Libre:700|Josefin+Sans:700|Lato:900|Montserrat:200,400,500,700,800" rel="stylesheet">
	<?php $this->helpers->analytics(); ?>
</head>

<body <?php $this->helpers->bodyClasses($view); ?>>
<?php $this->checkSuspensionHeader(); ?>
	<header>
		<div id="header">
			<div class="row">
				<div class="hdLeft col-1 fl">
					<a href="<?php echo URL ?>"> <img src="public/images/common/mainLogo.png" alt="<?php $this->info("company_name") ?> Main Logo"> </a>
				</div>
				<div class="hdRight col-2 fl">
					<div class="hdRightTop">
						<p> <img src="public/images/sprite.png" alt="email icon" class="bg-hdemail"> <span><?php $this->info(["email","mailto"]); ?></span> </p>
						<p> <img src="public/images/sprite.png" alt="phone icon" class="bg-hdphone"> <span><?php $this->info(["phone","tel"]); ?></span> </p>
					</div>
					<div class="hdRightBot">
						<nav>
							<a href="#" id="pull"><strong>MENU</strong></a>
							<ul>
								<li <?php $this->helpers->isActiveMenu("home"); ?>><a href="<?php echo URL ?>">HOME</a></li>
								<li <?php $this->helpers->isActiveMenu("about"); ?>><a href="<?php echo URL ?>about">ABOUT US</a></li>
								<li <?php $this->helpers->isActiveMenu("services"); ?>><a href="<?php echo URL ?>services">SERVICES</a></li>
								<li <?php $this->helpers->isActiveMenu("gallery"); ?>><a href="<?php echo URL ?>gallery">GALLERY</a></li>
								<li <?php $this->helpers->isActiveMenu("contact"); ?>><a href="<?php echo URL ?>contact">CONTACT US</a></li>
							</ul>
						</nav>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</header>

	<?php if($view == "home"):?>
		<div id="banner">
			<div class="row">
				<div class="bnrBox">
					<p>HARDWOOD FLOORS</p>
					<a href="<?php echo URL ?>gallery#content" class="btn">VIEW MORE</a>
				</div>
			</div>
		</div>
	<?php endif; ?>
